package iais.ukdw.com.dutani.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import iais.ukdw.com.dutani.R;
import iais.ukdw.com.dutani.model.LahanOnePetani;

public class LahanOnePetaniAdapter extends RecyclerView.Adapter<LahanOnePetaniAdapter.ViewHolder> {

    private List<LahanOnePetani> mLahan;

    public LahanOnePetaniAdapter(List<LahanOnePetani> mLahan) {
        this.mLahan = mLahan;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_lahan_petani_one, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LahanOnePetaniAdapter.ViewHolder holder, int i) {
        holder.txtIdLahan.setText("ID "+mLahan.get(i).getID_Lahan());
        holder.txtNamaLahan.setText(mLahan.get(i).getNama_lahan() + " ||");
        holder.txtLuasLahan.setText("Luas Lahan " + mLahan.get(i).getLuas_lahan() + "m2");
        holder.txtJenisLahan.setText(mLahan.get(i).getJenis_lahan());
        holder.txtDesa.setText("Desa " + mLahan.get(i).getDesa());
        holder.txtStatusOrganik.setText("Status "+mLahan.get(i).getStatus_organik() + " ||");
    }

    @Override
    public int getItemCount() {
        int tmpSize = 0;
        if(mLahan!=null)
            tmpSize = mLahan.size();
        return tmpSize;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener
    {
        private TextView txtIdLahan, txtNamaLahan, txtKoordX, txtKoordY, txtLuasLahan, txtJenisLahan, txtDesa, txtKec, txtKab, txtProv, txtStatusOrganik;

        public ViewHolder(View view) {
            super(view);
            txtIdLahan = (TextView) view.findViewById(R.id.txt_id_detil_lahan);
            txtNamaLahan = (TextView) view.findViewById(R.id.txt_nama_lahan);
            txtLuasLahan = (TextView) view.findViewById(R.id.txt_luas_lahan);
            txtJenisLahan = (TextView) view.findViewById(R.id.txt_jenis_lahan);
            txtDesa = (TextView) view.findViewById(R.id.txt_desa);
            txtStatusOrganik = (TextView) view.findViewById(R.id.txt_status_organik);
            view.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Pilih Aksi");
            menu.add(this.getAdapterPosition(), v.getId(), 0, "Detil Titik");//groupId, itemId, order, title
            menu.add(this.getAdapterPosition(), v.getId(), 0, "Ubah Data Lahan");
            menu.add(this.getAdapterPosition(), v.getId(), 0, "Hapus Data Lahan");
        }
    }

    public void clear(){
        mLahan.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<LahanOnePetani> newLahan){
        mLahan.addAll(newLahan);
        notifyDataSetChanged();
    }
}
