package iais.ukdw.com.dutani.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetilTitikLahan {
    @SerializedName("id_lahan")
    @Expose
    String id_lahan;

    @SerializedName("id_detail")
    @Expose
    String id_detail;

    @SerializedName("lat")
    @Expose
    String lat;

    @SerializedName("longt")
    @Expose
    String longt;

    @SerializedName("indeks")
    @Expose
    String indeks;

    public DetilTitikLahan(String id_lahan, String id_detail, String lat, String longt, String indeks) {
        this.id_lahan = id_lahan;
        this.id_detail = id_detail;
        this.lat = lat;
        this.longt = longt;
        this.indeks = indeks;
    }

    public String getId_lahan() {
        return id_lahan;
    }

    public void setId_lahan(String id_lahan) {
        this.id_lahan = id_lahan;
    }

    public String getId_detail() {
        return id_detail;
    }

    public void setId_detail(String id_detail) {
        this.id_detail = id_detail;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongt() {
        return longt;
    }

    public void setLongt(String longt) {
        this.longt = longt;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }
}
