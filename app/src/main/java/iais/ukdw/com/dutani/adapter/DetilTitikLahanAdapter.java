package iais.ukdw.com.dutani.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import iais.ukdw.com.dutani.R;
import iais.ukdw.com.dutani.model.DetilTitikLahan;

public class DetilTitikLahanAdapter extends RecyclerView.Adapter<DetilTitikLahanAdapter.ViewHolder> {

    private List<DetilTitikLahan> mLahan;

    public DetilTitikLahanAdapter(List<DetilTitikLahan> mLahan) {
        if(mLahan.get(0).getId_detail().equals("0"))
        {}else{
            this.mLahan = mLahan;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_detil_titik_lahan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtId.setText("TITIK " + mLahan.get(position).getId_detail());
        holder.txtLat.setText(mLahan.get(position).getLat() + ",");
        holder.txtLongt.setText(mLahan.get(position).getLongt());
    }

    @Override
    public int getItemCount() {
        int tmpSize = 0;
        if(mLahan!=null)
            tmpSize = mLahan.size();

        return tmpSize;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
//        implements View.OnCreateContextMenuListener
    {
        private TextView txtId, txtLat, txtLongt;

        public ViewHolder(View view) {
            super(view);
            txtId = (TextView) view.findViewById(R.id.txt_id_detil_lahan);
            txtLat = (TextView) view.findViewById(R.id.txt_lat);
            txtLongt = (TextView) view.findViewById(R.id.txt_longt);
            //view.setOnCreateContextMenuListener(this);
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), LahanOnePetani.class);
                    intent.putExtra("id_user",mLahan.get(getAdapterPosition()).getID_User());
                    intent.putExtra("bisa",mLahan.get(getAdapterPosition()).getBisa());
                    intent.putExtra("jml_lahan",mLahan.get(getAdapterPosition()).getJml_lahan());
                    intent.putExtra("jml_tercatat",mLahan.get(getAdapterPosition()).getJml_tercatat());
                    intent.putExtra("nama_user",mLahan.get(getAdapterPosition()).getNama_Petani());
                    v.getContext().startActivity(intent);
                }
            });*/
        }

        /*@Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Pilih Aksi");
            menu.add(0, v.getId(), 0, "Detil Titik");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "Ubah Data Lahan");
        }*/
    }

    public void clear(){
        mLahan.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<DetilTitikLahan> newTitik){
        mLahan.addAll(newTitik);
        notifyDataSetChanged();
    }
}
