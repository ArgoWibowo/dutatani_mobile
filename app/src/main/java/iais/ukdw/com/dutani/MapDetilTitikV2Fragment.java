package iais.ukdw.com.dutani;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MapDetilTitikV2Fragment extends Fragment implements LocationListener,
        OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "daftarTitik";

    private static final int LOCATION_REQUEST_CODE = 101;
    private static final int LOCATION_REQUEST_CODE_2 = 102;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<String> daftarTitik;

    private GoogleMap map;
    Location location = new Location("myMarker");;
    private MarkerOptions markerOptions;

    public MapDetilTitikV2Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    //Permission Granted
                }
                break;
        }
    }

    public static MapDetilTitikFragment newInstance(String param1, String param2, ArrayList<String> param3) {
        MapDetilTitikFragment fragment = new MapDetilTitikFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putStringArrayList(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            daftarTitik = getArguments().getStringArrayList(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, LOCATION_REQUEST_CODE);
        }
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_detil_titik, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById((R.id.fragmentMap));
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        SupportMapFragment mapFragment = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragmentMap));

        if(mapFragment != null) {
            FragmentManager fM = getFragmentManager();
            fM.beginTransaction().remove(mapFragment).commit();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if(daftarTitik.size() > 1){
            ArrayList<LatLng> coordList = new ArrayList<LatLng>();
            for(int i =0; i< daftarTitik.size(); i++){
                String[] separate = daftarTitik.get(i).split("\\|");
                coordList.add(new LatLng(Double.valueOf(separate[0]), Double.valueOf(separate[1])));
                if(i == daftarTitik.size()-1){
                    String[] first = daftarTitik.get(0).split("\\|");
                    coordList.add(new LatLng(Double.valueOf(first[0]), Double.valueOf(first[1])));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(separate[0]), Double.valueOf(separate[1])), 17));
                }
            }

            PolylineOptions polylineOptions = new PolylineOptions();

            // Create polyline options with existing LatLng ArrayList
            polylineOptions.addAll(coordList);
            polylineOptions
                    .width(5)
                    .color(Color.RED);

            // Adding multiple points in map using polyline and arraylist
            googleMap.addPolyline(polylineOptions);
        }else{
            ArrayList<LatLng> coordList = new ArrayList<LatLng>();
            for(int i =0; i< daftarTitik.size(); i++){
                String[] separate = daftarTitik.get(i).split("\\|");
                coordList.add(new LatLng(Double.valueOf(separate[0]), Double.valueOf(separate[1])));
                if(i == daftarTitik.size()-1){
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.valueOf(separate[0]), Double.valueOf(separate[1]))));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(separate[0]), Double.valueOf(separate[1])), 17));
                }
            }
        }

        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .draggable(true));
                location.setLatitude(latLng.latitude);
                location.setLongitude(latLng.longitude);
            }
        });

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                location.setLatitude(marker.getPosition().latitude);
                location.setLongitude(marker.getPosition().longitude);
            }
        });
    }

    public List<Double> getLatLong(){
        List<Double> result = Arrays.asList(location.getLatitude(),location.getLongitude());
        return result;
    }
}
