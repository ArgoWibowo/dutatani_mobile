package iais.ukdw.com.dutani.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLogin {
    @SerializedName("user")
    @Expose
    private String user;

    @SerializedName("kategori")
    @Expose
    private String kategori;

    public UserLogin(String user, String kategori) {
        this.setUser(user);
        this.setKategori(kategori);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
}
