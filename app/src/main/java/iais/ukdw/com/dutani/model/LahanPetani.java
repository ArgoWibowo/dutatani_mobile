package iais.ukdw.com.dutani.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LahanPetani {
    @SerializedName("ID_User")
    @Expose
    private String ID_User;

    @SerializedName("Nama_Petani")
    @Expose
    private String Nama_Petani;

    @SerializedName("jml_lahan")
    @Expose
    private String jml_lahan;

    @SerializedName("jml_tercatat")
    @Expose
    private String jml_tercatat;

    @SerializedName("bisa")
    @Expose
    private String bisa;

    public LahanPetani(String ID_User, String nama_Petani, String jml_lahan, String jml_tercatat, String bisa) {
        this.ID_User = ID_User;
        Nama_Petani = nama_Petani;
        this.jml_lahan = jml_lahan;
        this.jml_tercatat = jml_tercatat;
        this.bisa = bisa;
    }

    public String getID_User() {
        return ID_User;
    }

    public void setID_User(String ID_User) {
        this.ID_User = ID_User;
    }

    public String getNama_Petani() {
        return Nama_Petani;
    }

    public void setNama_Petani(String nama_Petani) {
        Nama_Petani = nama_Petani;
    }

    public String getJml_lahan() {
        return jml_lahan;
    }

    public void setJml_lahan(String jml_lahan) {
        this.jml_lahan = jml_lahan;
    }

    public String getJml_tercatat() {
        return jml_tercatat;
    }

    public void setJml_tercatat(String jml_tercatat) {
        this.jml_tercatat = jml_tercatat;
    }

    public String getBisa() {
        return bisa;
    }

    public void setBisa(String bisa) {
        this.bisa = bisa;
    }
}