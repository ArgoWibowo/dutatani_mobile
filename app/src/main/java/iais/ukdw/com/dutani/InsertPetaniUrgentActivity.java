package iais.ukdw.com.dutani;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import iais.ukdw.com.dutani.model.DefaultResult;
import iais.ukdw.com.dutani.network.GetDataService;
import iais.ukdw.com.dutani.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertPetaniUrgentActivity extends AppCompatActivity {

    private EditText txtIdUser, txtNama;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_petani_urgent);

        txtIdUser = (EditText) findViewById(R.id.edit_text_id_user);
        txtNama = (EditText) findViewById(R.id.edit_text_nama_petani);

        Button btnSimpan = (Button)findViewById(R.id.button_simpan_petani_urgent);
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isValid = true;

                //validation
                if(txtIdUser.getText().toString().matches("")){
                    txtIdUser.setError("Silahkan mengisi ID User");
                    isValid = false;
                }

                if(txtNama.getText().toString().matches("")){
                    txtNama.setError("Silahkan mengisi Nama Petani");
                    isValid = false;
                }

                if(isValid){
                    progressDialog = new ProgressDialog(InsertPetaniUrgentActivity.this);
                    progressDialog.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.insert_petani_urgent(
                            txtIdUser.getText().toString(),
                            txtNama.getText().toString()
                    );
                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            progressDialog.dismiss();
                            Toast.makeText(InsertPetaniUrgentActivity.this,"Berhasil Tambah Petani (Urgent)",Toast.LENGTH_LONG).show();
                            finish();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(InsertPetaniUrgentActivity.this,"Gagal Tambah Petani (Urgent), Coba Lagi",Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }
}
