package iais.ukdw.com.dutani;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import iais.ukdw.com.dutani.adapter.GambarMenuAdapter;

public class HomeFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_home, container, false);

        GridView gv = (GridView) fragmentView.findViewById(R.id.gViewMenu);

        gv.setAdapter(new GambarMenuAdapter(getContext()));
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        //Halaman Titik Lahan
                        Intent intent = new Intent(getActivity(), LahanPetaniAll.class);
                        intent.putExtra("menu_lahan","v1");
                        startActivity(intent);
                        break;
                    case 1:
                        //Halaman Titik Lahan Versi 2
                        Intent intent_lahan_add_v2 = new Intent(getActivity(), LahanPetaniAll.class);
                        intent_lahan_add_v2.putExtra("menu_lahan","v2");
                        startActivity(intent_lahan_add_v2);
                        break;
                    case 2:
                        //Input petani urgent
                        Intent intent_add_petani_urgent = new Intent(getActivity(), InsertPetaniUrgentActivity.class);
                        startActivity(intent_add_petani_urgent);
                        break;
                }
            }
        });

        return fragmentView;
    }

    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
