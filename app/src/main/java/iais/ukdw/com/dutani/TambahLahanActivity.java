package iais.ukdw.com.dutani;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

import iais.ukdw.com.dutani.model.DefaultResult;
import iais.ukdw.com.dutani.network.GetDataService;
import iais.ukdw.com.dutani.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahLahanActivity extends AppCompatActivity {

    private EditText txtNama;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_lahan);

        AutoCompleteTextView txtProv = (AutoCompleteTextView) findViewById(R.id.spinner_prov);
        ArrayAdapter myAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.prov_arr));
        txtProv.setAdapter(myAdapter);

        AutoCompleteTextView txtKab = (AutoCompleteTextView) findViewById(R.id.spinner_kab);
        ArrayAdapter myAdapterKab = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.kab_arr));
        txtKab.setAdapter(myAdapterKab);

        AutoCompleteTextView txtKec = (AutoCompleteTextView) findViewById(R.id.spinner_kec);
        ArrayAdapter myAdapterKec = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.kec_arr));
        txtKec.setAdapter(myAdapterKec);

        AutoCompleteTextView txtDesa = (AutoCompleteTextView) findViewById(R.id.spinner_desa);
        ArrayAdapter myAdapterDesa = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.desa_arr));
        txtDesa.setAdapter(myAdapterDesa);

        txtNama = (EditText) findViewById(R.id.edit_nama_petani);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // get data via the key
        final String valId = extras.getString("id_user");
        final String valNamaUser = extras.getString("nama_user");

        txtNama.setText(valNamaUser);

        //set maps
        Bundle bundle = new Bundle();
        bundle.putString("param1","");

        final Fragment fragment = new MapTambahTitikFragment();
        fragment.setArguments(bundle);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.mapViewLahan, fragment);
        fragmentTransaction.commit();

        Button btnSImpan = (Button) findViewById(R.id.btn_simpan_lahan);
        btnSImpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isValid = true;

                //get data from input
                EditText edNamaLahan = (EditText)findViewById(R.id.edit_nama_lahan);
                EditText edLuasLahan = (EditText)findViewById(R.id.edit_luas_lahan);
                Spinner spinnerJenisLahan = (Spinner) findViewById(R.id.spinner_jenis_lahan);
                TextView spinnerProv = (TextView) findViewById(R.id.spinner_prov);
                TextView spinnerKab = (TextView) findViewById(R.id.spinner_kab);
                TextView spinnerKec = (TextView) findViewById(R.id.spinner_kec);
                TextView spinenrDesa = (TextView) findViewById(R.id.spinner_desa);
                Spinner spinnerStatusOrganik = (Spinner) findViewById(R.id.spinner_status_organik);
                Spinner spinnerStatusLahan = (Spinner) findViewById(R.id.spinner_status_lahan);

                //check required
                if(edNamaLahan.getText().toString().matches("")){
                    edNamaLahan.setError("Silahkan mengisi nama lahan");
                    isValid = false;
                }
                if(edLuasLahan.getText().toString().matches("")){
                    edLuasLahan.setError("Silahkan mengisi luas lahan");
                    isValid = false;
                }
                if(spinenrDesa.getText().toString().matches("")){
                    spinenrDesa.setError("Silahkan mengisi Desa");
                    isValid = false;
                }

                if(isValid){
                    progressDialog = new ProgressDialog(TambahLahanActivity.this);
                    progressDialog.show();

                    List<Double> posisi = ((MapTambahTitikFragment) fragment).getLatLong();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.insert_lahan(valNamaUser,
                            edNamaLahan.getText().toString(),
                            edLuasLahan.getText().toString(),
                            spinnerJenisLahan.getSelectedItem().toString(),
                            spinnerProv.getText().toString(),
                            spinnerKab.getText().toString(),
                            spinnerKec.getText().toString(),
                            spinenrDesa.getText().toString(),
                            spinnerStatusOrganik.getSelectedItem().toString(),
                            spinnerStatusLahan.getSelectedItem().toString(),
                            valId,
                            posisi.get(0).toString(),
                            posisi.get(1).toString()
                    );
                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            progressDialog.dismiss();
                            Toast.makeText(TambahLahanActivity.this,"Berhasil Tambah Lahan",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(TambahLahanActivity.this,"Gagal Tambah Lahan, Coba Lagi",Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }
}
