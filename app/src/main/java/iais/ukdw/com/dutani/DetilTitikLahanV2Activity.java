package iais.ukdw.com.dutani;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import iais.ukdw.com.dutani.adapter.DetilTitikLahanAdapter;
import iais.ukdw.com.dutani.model.DefaultResult;
import iais.ukdw.com.dutani.model.DetilTitikLahan;
import iais.ukdw.com.dutani.network.GetDataService;
import iais.ukdw.com.dutani.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetilTitikLahanV2Activity extends AppCompatActivity {
    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private DetilTitikLahanAdapter adapter;
    private String id_lahan;
    List<DetilTitikLahan> listTitik;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_titik_lahan_v2);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        id_lahan = extras.getString("id_lahan");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<DetilTitikLahan>> call = service.getDetilTitikLahan(id_lahan);
        call.enqueue(new Callback<List<DetilTitikLahan>>() {
            @Override
            public void onResponse(Call<List<DetilTitikLahan>> call, Response<List<DetilTitikLahan>> response) {
                progressDialog.dismiss();
                generateData(response.body());
                listTitik = response.body();
                ArrayList<String> convertResponse = new ArrayList<String>(listTitik.size());
                for(int i =0; i< listTitik.size(); i++){
                    convertResponse.add(listTitik.get(i).getLat() + "|" + listTitik.get(i).getLongt());
                }
                Bundle bundle = new Bundle();
                bundle.putString("param1","");
                bundle.putStringArrayList("daftarTitik",convertResponse);

                fragment = new MapDetilTitikV2Fragment();
                fragment.setArguments(bundle);

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.mapViewLahan, fragment);
                fragmentTransaction.commit();
            }

            @Override
            public void onFailure(Call<List<DetilTitikLahan>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DetilTitikLahanV2Activity.this,"Login Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
            }
        });

        Button btnTambahTitik = (Button)findViewById(R.id.btn_simpan_titik);
        btnTambahTitik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Double> posisi = ((MapDetilTitikV2Fragment) fragment).getLatLong();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<DetilTitikLahan>> call = service.insert_one_titik_lahanv2(id_lahan,"0",posisi.get(0).toString(),posisi.get(1).toString());
                call.enqueue(new Callback<List<DetilTitikLahan>>() {
                    @Override
                    public void onResponse(Call<List<DetilTitikLahan>> call, Response<List<DetilTitikLahan>> response) {
                        progressDialog.dismiss();
                        Toast.makeText(DetilTitikLahanV2Activity.this,"Berhasil Tambah Titik",Toast.LENGTH_LONG).show();

                        //refresh map
                        listTitik = response.body();
                        ArrayList<String> convertResponse = new ArrayList<String>(listTitik.size());
                        for(int i =0; i< listTitik.size(); i++){
                            convertResponse.add(listTitik.get(i).getLat() + "|" + listTitik.get(i).getLongt());
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString("param1","");
                        bundle.putStringArrayList("daftarTitik",convertResponse);

                        fragment = new MapDetilTitikV2Fragment();
                        fragment.setArguments(bundle);

                        FragmentManager fm = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fm.beginTransaction();
                        fragmentTransaction.replace(R.id.mapViewLahan, fragment);
                        fragmentTransaction.commit();

                        generateData(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<DetilTitikLahan>> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(DetilTitikLahanV2Activity.this,"Gagal Tambah Titik, Coba Lagi",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    public void generateData(List<DetilTitikLahan> lahanList){
        recyclerView = (RecyclerView) findViewById(R.id.rv_detil_titik_lahan);
        adapter = new DetilTitikLahanAdapter(lahanList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
