package iais.ukdw.com.dutani;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import iais.ukdw.com.dutani.adapter.LahanOnePetaniAdapter;
import iais.ukdw.com.dutani.adapter.LahanPetaniAllAdapter;
import iais.ukdw.com.dutani.model.DetilTitikLahan;
import iais.ukdw.com.dutani.model.LahanPetani;
import iais.ukdw.com.dutani.network.GetDataService;
import iais.ukdw.com.dutani.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LahanOnePetani extends AppCompatActivity {

    private TextView txtId, txtNama, txtJmlLahan, txtJmlTercatat, txtBisa;
    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private LahanOnePetaniAdapter adapter;
    List<iais.ukdw.com.dutani.model.LahanOnePetani> lahanList;
    String valId, valNamaUser, menu_lahan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lahan_one_petani);

        txtId = (TextView) findViewById(R.id.txt_id_user_one_petani);
        txtNama = (TextView) findViewById(R.id.txt_nama_one_petani);
        txtJmlLahan = (TextView) findViewById(R.id.txt_jml_lahan_one_petani);
        txtJmlTercatat = (TextView) findViewById(R.id.txt_jml_tercatat_one_petani);
        txtBisa = (TextView) findViewById(R.id.txt_bisa_one_petani);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // get data via the key
        valId = extras.getString("id_user");
        String valBisa = extras.getString("bisa");
        String valJmlLahan = extras.getString("jml_lahan");
        String valJmlTercatat = extras.getString("jml_tercatat");
        valNamaUser = extras.getString("nama_user");
        menu_lahan = extras.getString("menu_lahan");

        if(valId != null) {
            txtId.setText(valId);
        }
        if(valNamaUser != null){
            txtNama.setText("Nama: " + valNamaUser);
        }
        if(valJmlLahan != null){
            txtJmlLahan.setText("Total Jumlah Lahan: " + valJmlLahan);
        }
        if(valJmlTercatat != null){
            txtJmlTercatat.setText("Jumlah Lahan yang sudah teridentifikasi: " + valJmlTercatat);
        }
        if(valBisa != null){
            txtBisa.setText("Lahan yang bisa ditambahkan: " + valBisa);
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<iais.ukdw.com.dutani.model.LahanOnePetani>> call = service.getLahanOnePetaniSimple(valId);
        call.enqueue(new Callback<List<iais.ukdw.com.dutani.model.LahanOnePetani>>() {
            @Override
            public void onResponse(Call<List<iais.ukdw.com.dutani.model.LahanOnePetani>> call, Response<List<iais.ukdw.com.dutani.model.LahanOnePetani>> response) {
                progressDialog.dismiss();
                lahanList = response.body();
                generateData(response.body());
            }

            @Override
            public void onFailure(Call<List<iais.ukdw.com.dutani.model.LahanOnePetani>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LahanOnePetani.this,"Login Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.rvLahanOnePetaniSimple);

        Button btnTambahLahan = (Button) findViewById(R.id.btn_tambah_lahan);
        btnTambahLahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LahanOnePetani.this, TambahLahanActivity.class);
                intent.putExtra("id_user",valId);
                intent.putExtra("nama_user",valNamaUser);
                startActivity(intent);
            }
        });

        registerForContextMenu(recyclerView);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        txtId = (TextView) findViewById(R.id.txt_id_user_one_petani);
        txtNama = (TextView) findViewById(R.id.txt_nama_one_petani);
        txtJmlLahan = (TextView) findViewById(R.id.txt_jml_lahan_one_petani);
        txtJmlTercatat = (TextView) findViewById(R.id.txt_jml_tercatat_one_petani);
        txtBisa = (TextView) findViewById(R.id.txt_bisa_one_petani);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // get data via the key
        valId = extras.getString("id_user");
        String valBisa = extras.getString("bisa");
        String valJmlLahan = extras.getString("jml_lahan");
        String valJmlTercatat = extras.getString("jml_tercatat");
        valNamaUser = extras.getString("nama_user");
        menu_lahan = extras.getString("menu_lahan");

        if(valId != null) {
            txtId.setText(valId);
        }
        if(valNamaUser != null){
            txtNama.setText("Nama: " + valNamaUser);
        }
        if(valJmlLahan != null){
            txtJmlLahan.setText("Total Jumlah Lahan: " + valJmlLahan);
        }
        if(valJmlTercatat != null){
            txtJmlTercatat.setText("Jumlah Lahan yang sudah teridentifikasi: " + valJmlTercatat);
        }
        if(valBisa != null){
            txtBisa.setText("Lahan yang bisa ditambahkan: " + valBisa);
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<iais.ukdw.com.dutani.model.LahanOnePetani>> call = service.getLahanOnePetaniSimple(valId);
        call.enqueue(new Callback<List<iais.ukdw.com.dutani.model.LahanOnePetani>>() {
            @Override
            public void onResponse(Call<List<iais.ukdw.com.dutani.model.LahanOnePetani>> call, Response<List<iais.ukdw.com.dutani.model.LahanOnePetani>> response) {
                progressDialog.dismiss();
                lahanList = response.body();
                generateData(response.body());
            }

            @Override
            public void onFailure(Call<List<iais.ukdw.com.dutani.model.LahanOnePetani>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LahanOnePetani.this,"Login Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.rvLahanOnePetaniSimple);
    }

    public void generateData(List<iais.ukdw.com.dutani.model.LahanOnePetani> lahanList){
        recyclerView = (RecyclerView) findViewById(R.id.rvLahanOnePetaniSimple);
        adapter = new LahanOnePetaniAdapter(lahanList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        iais.ukdw.com.dutani.model.LahanOnePetani lahan = lahanList.get(item.getGroupId());
        if (item.getTitle() == "Detil Titik"){
            if(menu_lahan.equals("v1")){
                Intent intent = new Intent(LahanOnePetani.this, DetilTitikLahanActivity.class);
                intent.putExtra("id_user",valId);
                intent.putExtra("nama_user",valNamaUser);
                intent.putExtra("id_lahan",lahan.getID_Lahan());
                startActivity(intent);
            }else if (menu_lahan.equals("v2")){
                Intent intent = new Intent(LahanOnePetani.this, DetilTitikLahanV2Activity.class);
                intent.putExtra("id_user",valId);
                intent.putExtra("nama_user",valNamaUser);
                intent.putExtra("id_lahan",lahan.getID_Lahan());
                startActivity(intent);
            }
        }else if(item.getTitle() == "Ubah Data Lahan"){
            Toast.makeText(this, "Ubah Lahan " + lahan.getID_Lahan(),Toast.LENGTH_LONG).show();
        }else if(item.getTitle() == "Ubah Data Lahan") {
            Toast.makeText(this, "Hapus Lahan " + lahan.getID_Lahan(), Toast.LENGTH_LONG).show();
        }
        return super.onContextItemSelected(item);
    }
}
