package iais.ukdw.com.dutani.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LahanOnePetani {

    @SerializedName("ID_Lahan")
    @Expose
    String ID_Lahan;

    @SerializedName("nama_lahan")
    @Expose
    String nama_lahan;

    @SerializedName("luas_lahan")
    @Expose
    String luas_lahan;

    @SerializedName("jenis_lahan")
    @Expose
    String jenis_lahan;

    @SerializedName("Desa")
    @Expose
    String Desa;

    @SerializedName("status_organik")
    @Expose
    String status_organik;

    public LahanOnePetani(String ID_Lahan, String nama_lahan, String luas_lahan, String jenis_lahan, String desa, String status_organik) {
        this.ID_Lahan = ID_Lahan;
        this.nama_lahan = nama_lahan;
        this.luas_lahan = luas_lahan;
        this.jenis_lahan = jenis_lahan;
        Desa = desa;
        this.status_organik = status_organik;
    }

    public String getID_Lahan() {
        return ID_Lahan;
    }

    public void setID_Lahan(String ID_Lahan) {
        this.ID_Lahan = ID_Lahan;
    }

    public String getNama_lahan() {
        return nama_lahan;
    }

    public void setNama_lahan(String nama_lahan) {
        this.nama_lahan = nama_lahan;
    }

    public String getLuas_lahan() {
        return luas_lahan;
    }

    public void setLuas_lahan(String luas_lahan) {
        this.luas_lahan = luas_lahan;
    }

    public String getJenis_lahan() {
        return jenis_lahan;
    }

    public void setJenis_lahan(String jenis_lahan) {
        this.jenis_lahan = jenis_lahan;
    }

    public String getDesa() {
        return Desa;
    }

    public void setDesa(String desa) {
        Desa = desa;
    }

    public String getStatus_organik() {
        return status_organik;
    }

    public void setStatus_organik(String status_organik) {
        this.status_organik = status_organik;
    }
}
