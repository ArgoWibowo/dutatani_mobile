package iais.ukdw.com.dutani;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import iais.ukdw.com.dutani.model.DefaultResult;
import iais.ukdw.com.dutani.model.DetilTitikLahan;
import iais.ukdw.com.dutani.model.UserLogin;
import iais.ukdw.com.dutani.network.GetDataService;
import iais.ukdw.com.dutani.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahDetilTitikLahanActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_detil_titik_lahan);

        Bundle bundle = new Bundle();
        bundle.putString("param1","");

        final Fragment fragment = new TambahDetilTitikFragment();
        fragment.setArguments(bundle);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.mapViewLahan, fragment);
        fragmentTransaction.commit();

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // get data idlahan from prev activity via the key
        final String valIdLahan = extras.getString("id_lahan");

        Button btnTambahTitik = (Button)findViewById(R.id.btn_simpan_titik);
        btnTambahTitik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(TambahDetilTitikLahanActivity.this);
                progressDialog.show();

                List<Double> posisi = ((TambahDetilTitikFragment) fragment).getLatLong();
                //Toast.makeText(TambahDetilTitikLahanActivity.this,posisi.get(0).toString(),Toast.LENGTH_LONG).show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.insert_one_titik_lahan(valIdLahan,"0",posisi.get(0).toString(),posisi.get(1).toString());
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        progressDialog.dismiss();
                        Toast.makeText(TambahDetilTitikLahanActivity.this,"Berhasil Tambah Titik",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(TambahDetilTitikLahanActivity.this,"Gagal Tambah Titik, Coba Lagi",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
