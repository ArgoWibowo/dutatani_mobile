package iais.ukdw.com.dutani.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import iais.ukdw.com.dutani.R;

public class GambarMenuAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater=null;
    //deklarasi Context


    public GambarMenuAdapter(Context c) {

        context = c;
        //Constructor
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {

        return idGambar.length;
    }

    public Object getItem(int position) {


        return null;
    }

    public long getItemId(int position) {

        return 0;

    }

    public class Holder
    {
        TextView os_text;
        ImageView os_img;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.single_grid_home, null);
        holder.os_text =(TextView) rowView.findViewById(R.id.title_texts);
        holder.os_img =(ImageView) rowView.findViewById(R.id.title_images);

        holder.os_text.setText(textMenu[position]);
        holder.os_img.setImageResource(idGambar[position]);

        return rowView;
    }

    private Integer[] idGambar = {
            R.drawable.ic_lahan_add,
            R.drawable.ic_lahan_add_2,
            R.drawable.add_petani_urgent
    };

    private String[] textMenu = {
            "Data Lahan",
            "Input Lahan",
            "Input Petani (Urgent)"
    };
}
