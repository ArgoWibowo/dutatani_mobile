package iais.ukdw.com.dutani.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import iais.ukdw.com.dutani.LahanOnePetani;
import iais.ukdw.com.dutani.R;
import iais.ukdw.com.dutani.model.LahanPetani;
import java.util.List;

public class LahanPetaniAllAdapter extends RecyclerView.Adapter<LahanPetaniAllAdapter.ViewHolder> {

    private List<LahanPetani> mLahan;
    private String menu_lahan;

    public LahanPetaniAllAdapter(List<LahanPetani> mLahan, String menu_lahan) {
        this.mLahan = mLahan;
        this.menu_lahan = menu_lahan;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_lahan_petani_all, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.txtId.setText(mLahan.get(position).getID_User());
        holder.txtNama.setText("Nama: " + mLahan.get(position).getNama_Petani());
        holder.txtJmlTercatat.setText("Jumlah Lahan yang sudah teridentifikasi: " + mLahan.get(position).getJml_tercatat());
        holder.txtJmlLahan.setText("Total Jumlah Lahan: " + mLahan.get(position).getJml_lahan());
        holder.txtBisa.setText("Lahan yang bisa ditambahkan: " + mLahan.get(position).getBisa());
    }

    @Override
    public int getItemCount() {
        int tmpSize = 0;
        if(mLahan!=null)
        tmpSize = mLahan.size();

        return tmpSize;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
//        implements View.OnCreateContextMenuListener
    {
        private TextView txtId, txtNama, txtJmlLahan, txtJmlTercatat, txtBisa;

        public ViewHolder(View view) {
            super(view);
            txtId = (TextView) view.findViewById(R.id.txt_id_user);
            txtNama = (TextView) view.findViewById(R.id.txt_nama_petani);
            txtJmlLahan = (TextView) view.findViewById(R.id.txt_jml_lahan);
            txtJmlTercatat = (TextView) view.findViewById(R.id.txt_jml_tercatat);
            txtBisa = (TextView) view.findViewById(R.id.txt_bisa);
            //view.setOnCreateContextMenuListener(this);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), LahanOnePetani.class);
                    intent.putExtra("id_user",mLahan.get(getAdapterPosition()).getID_User());
                    intent.putExtra("bisa",mLahan.get(getAdapterPosition()).getBisa());
                    intent.putExtra("jml_lahan",mLahan.get(getAdapterPosition()).getJml_lahan());
                    intent.putExtra("jml_tercatat",mLahan.get(getAdapterPosition()).getJml_tercatat());
                    intent.putExtra("nama_user",mLahan.get(getAdapterPosition()).getNama_Petani());
                    intent.putExtra("menu_lahan",menu_lahan);
                    v.getContext().startActivity(intent);
                }
            });
        }

        /*@Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Pilih Aksi");
            menu.add(0, v.getId(), 0, "Detil Titik");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "Ubah Data Lahan");
        }*/
    }

    public void clear(){
        mLahan.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<LahanPetani> newLahan){
        mLahan.addAll(newLahan);
        notifyDataSetChanged();
    }
}
