package iais.ukdw.com.dutani;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.List;

import iais.ukdw.com.dutani.adapter.LahanPetaniAllAdapter;
import iais.ukdw.com.dutani.model.LahanPetani;
import iais.ukdw.com.dutani.model.UserLogin;
import iais.ukdw.com.dutani.network.GetDataService;
import iais.ukdw.com.dutani.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LahanPetaniAll extends AppCompatActivity {

    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private LahanPetaniAllAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lahan_petani_all);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // get data idlahan from prev activity via the key
        final String menu_lahan = extras.getString("menu_lahan");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<LahanPetani>> call = service.getLahanPetaniAll("");
        call.enqueue(new Callback<List<LahanPetani>>() {
            @Override
            public void onResponse(Call<List<LahanPetani>> call, Response<List<LahanPetani>> response) {
                progressDialog.dismiss();
                generateData(response.body(),menu_lahan);
            }

            @Override
            public void onFailure(Call<List<LahanPetani>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LahanPetaniAll.this,"Login Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
            }
        });

        final SearchView searchView = (SearchView) findViewById(R.id.search_petani);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                progressDialog = new ProgressDialog(LahanPetaniAll.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<LahanPetani>> call = service.getLahanPetaniAll(searchView.getQuery().toString());
                call.enqueue(new Callback<List<LahanPetani>>() {
                    @Override
                    public void onResponse(Call<List<LahanPetani>> call, Response<List<LahanPetani>> response) {
                        progressDialog.dismiss();
                        generateData(response.body(),menu_lahan);
                    }

                    @Override
                    public void onFailure(Call<List<LahanPetani>> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(LahanPetaniAll.this,"Login Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
                    }
                });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        // get data idlahan from prev activity via the key
        final String menu_lahan = extras.getString("menu_lahan");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<LahanPetani>> call = service.getLahanPetaniAll("");
        call.enqueue(new Callback<List<LahanPetani>>() {
            @Override
            public void onResponse(Call<List<LahanPetani>> call, Response<List<LahanPetani>> response) {
                progressDialog.dismiss();
                generateData(response.body(),menu_lahan);
            }

            @Override
            public void onFailure(Call<List<LahanPetani>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LahanPetaniAll.this,"Login Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
            }
        });

        final SearchView searchView = (SearchView) findViewById(R.id.search_petani);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                progressDialog = new ProgressDialog(LahanPetaniAll.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<LahanPetani>> call = service.getLahanPetaniAll(searchView.getQuery().toString());
                call.enqueue(new Callback<List<LahanPetani>>() {
                    @Override
                    public void onResponse(Call<List<LahanPetani>> call, Response<List<LahanPetani>> response) {
                        progressDialog.dismiss();
                        generateData(response.body(),menu_lahan);
                    }

                    @Override
                    public void onFailure(Call<List<LahanPetani>> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(LahanPetaniAll.this,"Ambil Data Petani Gagal, Coba Lagi",Toast.LENGTH_LONG).show();
                    }
                });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    public void generateData(List<LahanPetani> lahanList, String menu_lahan){
        recyclerView = (RecyclerView) findViewById(R.id.rvLahanPetaniAll);
        adapter = new LahanPetaniAllAdapter(lahanList, menu_lahan);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
